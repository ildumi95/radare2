Source: radare2
Section: devel
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>,
           Sebastian Reichel <sre@debian.org>
Build-Depends: debhelper-compat (= 13),
               libcapstone-dev,
               libgmp-dev,
               libkvm-dev [kfreebsd-any],
               libmagic-dev,
               libuv1-dev,
               libxxhash-dev,
               libzip-dev,
               liblz4-dev,
               freebsd-glue,
               meson,
               pkg-config,
               python3:any,
               python3-setuptools,
               zlib1g-dev
Standards-Version: 4.5.1
Rules-Requires-Root: no
Homepage: https://www.radare.org
Vcs-Browser: https://salsa.debian.org/pkg-security-team/radare2
Vcs-Git: https://salsa.debian.org/pkg-security-team/radare2.git

Package: radare2
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Recommends: libradare2-dev
Description: free and advanced command line hexadecimal editor
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (radare) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.

Package: libradare2-5.0.0
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, libradare2-common (>= ${source:Version})
Multi-Arch: same
Description: libraries from the radare2 suite
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (radare) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.
 .
 This package provides the libraries from radare2.

Package: libradare2-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends}, libradare2-5.0.0 (= ${binary:Version}),
         libcapstone-dev, libmagic-dev, libuv1-dev, liblz4-dev,
         libzip-dev,
Description: devel files from the radare2 suite
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (radare) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.
 .
 This package provides the devel files from radare2.

Package: libradare2-common
Multi-Arch: foreign
Architecture: all
Depends: ${misc:Depends}
Description: arch independent files from the radare2 suite
 The project aims to create a complete, portable, multi-architecture,
 unix-like toolchain for reverse engineering.
 .
 It is composed by an hexadecimal editor (radare) with a wrapped IO
 layer supporting multiple backends for local/remote files, debugger
 (OS X, BSD, Linux, W32), stream analyzer, assembler/disassembler (rasm)
 for x86, ARM, PPC, m68k, Java, MSIL, SPARC, code analysis modules and
 scripting facilities. A bindiffer named radiff, base converter (rax),
 shellcode development helper (rasc), a binary information extractor
 supporting PE, mach0, ELF, class, etc. named rabin, and a block-based
 hash utility called rahash.
 .
 This package provides the arch independent files from radare2.
