radare2 (5.0.0+dfsg-1) unstable; urgency=medium

  [ Helmut Grohne ]
  * Annotate Build-Depends: python3 with :any. (Closes: #973451)

  [ Sebastian Reichel ]
  * New upstream release
  * Bump Standards Version to 4.5.1
  * Bump debhelper-compat level to 13
  * Add 'Rules-Requires-Root: no'

 -- Sebastian Reichel <sre@debian.org>  Mon, 04 Jan 2021 02:33:20 +0100

radare2 (4.3.1+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Sebastian Reichel <sre@debian.org>  Sun, 15 Mar 2020 19:36:29 +0100

radare2 (4.2.1+dfsg-2) unstable; urgency=medium

  * Add ${SOURCE_DATE_EPOCH} support for meson based builds
    to fix reproducible builds

 -- Sebastian Reichel <sre@debian.org>  Thu, 20 Feb 2020 20:30:24 +0100

radare2 (4.2.1+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Closes: #947791 (CVE-2019-19590)
   - Closes: #947402 (CVE-2019-19647)
  * Remove buildsystem hack, which removed patch level from shared
    object. We now simply expose the version in the same was as
    upstream. This reduces maintenance overhead and we usually only
    package one patch level release anyways.
  * Move compat level into debian/control
  * Bump Standards Version to 4.5.0
  * Add Andrej Shadura as Uploader/Co-Maintainer

 -- Sebastian Reichel <sre@debian.org>  Fri, 14 Feb 2020 00:16:22 +0100

radare2 (4.0.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Add libzip-dev dependency to libradare2-dev (Closes: #943857)

 -- Sebastian Reichel <sre@debian.org>  Fri, 03 Jan 2020 15:01:56 +0100

radare2 (3.9.0+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Sebastian Reichel <sre@debian.org>  Sun, 22 Sep 2019 21:45:47 +0200

radare2 (3.8.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Closes: #930344 (CVE-2019-12790)
   - Closes: #930510 (CVE-2019-12802)
   - Closes: #930590 (CVE-2019-12829)
   - Closes: #930704 (CVE-2019-12865)
   - Closes: #934204 (CVE-2019-14745)
  * Do not install NSFW fortunes messages

 -- Sebastian Reichel <sre@debian.org>  Wed, 04 Sep 2019 20:07:17 +0200

radare2 (3.2.1+dfsg-5) unstable; urgency=medium

  * Team upload
  * Add missing -dev dependencies (Closes: #923341)

 -- Hilko Bengen <bengen@debian.org>  Sat, 02 Mar 2019 14:39:45 +0100

radare2 (3.2.1+dfsg-4) unstable; urgency=medium

  * Add dependency to freebsd-glue on kfreebsd, which is required
    explicitly by libuv1-dev until #921424 is fixed in libuv.

 -- Sebastian Reichel <sre@debian.org>  Tue, 05 Feb 2019 12:06:09 +0100

radare2 (3.2.1+dfsg-3) unstable; urgency=medium

  * Switch to meson build system
  * Drop unused build depends

 -- Sebastian Reichel <sre@debian.org>  Tue, 05 Feb 2019 04:10:18 +0100

radare2 (3.2.1+dfsg-2) unstable; urgency=medium

  * Team upload
  * Remove recently-added libssl-dev build-dependency due to
    incompatibility with code licensed under GPL

 -- Hilko Bengen <bengen@debian.org>  Sat, 26 Jan 2019 00:32:42 +0100

radare2 (3.2.1+dfsg-1) unstable; urgency=medium

  * Team upload
  * New upstream version 3.2.1+dfsg
    - Fixes CVE-2018-20457, CVE-2018-20459 (Closes: #917322)
  * Drop patch that has been integrated upstream
  * Bump Debhelper compat level
  * Bump Standards-Version
  * Bump libradare2 SONAME
  * Add libssl-dev build-dependency

 -- Hilko Bengen <bengen@debian.org>  Wed, 23 Jan 2019 23:53:52 +0100

radare2 (3.1.2+dfsg-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * 0003-Move-endif-HAVE_PTRACE-after-functions-which-use-it.patch: Fix FTBFS
    when building with --disable-debugger. (Closes: #915857)

 -- Iain Lane <iain@orangesquash.org.uk>  Tue, 11 Dec 2018 11:34:51 +0000

radare2 (3.1.2+dfsg-1) unstable; urgency=medium

  * New upstream bugfix release

 -- Sebastian Reichel <sre@debian.org>  Tue, 04 Dec 2018 05:00:38 +0100

radare2 (3.1.0+dfsg-1) unstable; urgency=medium

  * Build using system libxxhash instead of bundled one
  * Replace libgmp3-dev with libgmp-dev
  * Build with libuv support

 -- Sebastian Reichel <sre@debian.org>  Mon, 26 Nov 2018 15:50:20 +0100

radare2 (3.0.1+dfsg-1) unstable; urgency=medium

  * New upstream bugfix release

 -- Sebastian Reichel <sre@debian.org>  Sat, 20 Oct 2018 21:42:15 +0200

radare2 (3.0.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - PATH_MAX usage has been removed (Closes: #879028)

 -- Sebastian Reichel <sre@debian.org>  Wed, 17 Oct 2018 00:23:17 +0200

radare2 (2.9.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Simplify DEBUG_SUPPORT conditionals in debian/rules (Closes: #905768)
  * Update Debian Standards Version to 4.2.1

 -- Sebastian Reichel <sre@debian.org>  Tue, 04 Sep 2018 16:51:13 +0200

radare2 (2.8.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Update Debian Standards Version to 4.1.5

 -- Sebastian Reichel <sre@debian.org>  Wed, 08 Aug 2018 22:37:49 +0200

radare2 (2.7.0+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Sebastian Reichel <sre@debian.org>  Tue, 10 Jul 2018 00:04:16 +0200

radare2 (2.6.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Switch to compat level 11
  * Backport a few patches to fix build with system libraries

 -- Sebastian Reichel <sre@debian.org>  Tue, 10 Apr 2018 16:29:24 +0200

radare2 (2.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release

 -- Sebastian Reichel <sre@debian.org>  Mon, 05 Mar 2018 21:32:57 +0100

radare2 (2.3.0+dfsg-2) unstable; urgency=medium

  * Add missing libcapstone-dev dependency to libradare2-dev
  * Fix Vcs urls

 -- Sebastian Reichel <sre@debian.org>  Fri, 23 Feb 2018 11:38:59 +0100

radare2 (2.3.0+dfsg-1) unstable; urgency=medium

  * New upstream release
  * Switch to package maintence from alioth to salsa
  * Make package team-maintained under pkg-security-team umbrella
  * Update Debian Standards Version to 4.1.3
  * Fix priority-extra-is-replaced-by-priority-optional
  * Update debian/copyright, debian/watch to use native support
    for cleaning up the orig tarball and drop the get-orig-source
    target from debian/rules.

 -- Sebastian Reichel <sre@debian.org>  Wed, 14 Feb 2018 17:48:06 +0100

radare2 (2.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Fix for CVE-2017-15368 (Closes: #878767)
     The wasm_dis function in libr/asm/arch/wasm/wasm.c in radare2 2.0.0
     allows remote attackers to cause a denial of service (stack-based
     buffer over-read and application crash) or possibly have unspecified
     other impact via a crafted WASM file that triggers an incorrect
     r_hex_bin2str call.
   - Fix for CVE-2017-15385 (Closes: #879119)
     The store_versioninfo_gnu_verdef function in libr/bin/format/elf/elf.c
     in radare2 2.0.0 allows remote attackers to cause a denial of service
     (r_read_le16 invalid write and application crash) or possibly have
     unspecified other impact via a crafted ELF file.
   - Fix for CVE-2017-15932 (Closes: #880024)
     In radare2 2.0.1, an integer exception (negative number leading to an
     invalid memory access) exists in store_versioninfo_gnu_verdef() in
     libr/bin/format/elf/elf.c via crafted ELF files when parsing the ELF
     version on 32bit systems.
   - Fix for CVE-2017-15931 (Closes: #880025)
     In radare2 2.0.1, an integer exception (negative number leading to an
     invalid memory access) exists in store_versioninfo_gnu_verneed() in
     libr/bin/format/elf/elf.c via crafted ELF files on 32bit systems.
   - Fix for CVE-2017-16359 (Closes: #880616)
     In radare 2.0.1, a pointer wraparound vulnerability exists in
     store_versioninfo_gnu_verdef() in libr/bin/format/elf/elf.c.
   - Fix for CVE-2017-16358 (Closes: #880619)
     In radare 2.0.1, an out-of-bounds read vulnerability exists in
     string_scan_range() in libr/bin/bin.c when doing a string search.
   - Fix for CVE-2017-16357 (Closes: #880620)
     In radare 2.0.1, a memory corruption vulnerability exists in
     store_versioninfo_gnu_verdef() and store_versioninfo_gnu_verneed() in
     libr/bin/format/elf/elf.c, as demonstrated by an invalid free. This
     error is due to improper sh_size validation when allocating memory.
   - Fix for CVE-2017-16805 (Closes: #882134)
     In radare2 2.0.1, libr/bin/dwarf.c allows remote attackers to cause a
     denial of service (invalid read and application crash) via a crafted
     ELF file, related to r_bin_dwarf_parse_comp_unit in dwarf.c and
     sdb_set_internal in shlr/sdb/src/sdb.c.
  * Update Debian Standards Version to 4.1.1

 -- Sebastian Reichel <sre@debian.org>  Mon, 27 Nov 2017 16:14:43 +0100

radare2 (2.0.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Fix for CVE-2017-9761 (Closes: #869428)
     The find_eoq function in libr/core/cmd.c in radare2 1.5.0 allows remote
     attackers to cause a denial of service (heap-based out-of-bounds read
     and application crash) via a crafted binary file.
   - Fix for CVE-2017-9762 (Closes: #869426)
     The cmd_info function in libr/core/cmd_info.c in radare2 1.5.0 allows
     remote attackers to cause a denial of service (use-after-free and
     application crash) via a crafted binary file.
   - Fix for CVE-2017-9763 (Closes: #869423)
     The grub_ext2_read_block function in fs/ext2.c in GNU GRUB before
     2013-11-12, as used in shlr/grub/fs/ext2.c in radare2 1.5.0, allows
     remote attackers to cause a denial of service (excessive stack use and
     application crash) via a crafted binary file, related to use of a
     variable-size stack array.
  * remove broken r2-indent symlink (Closes: #874524)
  * install upstream's zsh completion files

 -- Sebastian Reichel <sre@debian.org>  Wed, 11 Oct 2017 16:20:18 +0200

radare2 (1.6.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Fix for CVE-2017-9520 (Closes: #864533)
     The r_config_set function in libr/config/config.c in radare2 1.5.0
     allows remote attackers to cause a denial of service (use-after-free
     and application crash) via a crafted DEX file.
   - Fix for CVE-2017-9949 (Closes: #866068)
     The grub_memmove function in shlr/grub/kern/misc.c in radare2 1.5.0
     allows remote attackers to cause a denial of service (stack-based
     buffer underflow and application crash) or possibly have unspecified
     other impact via a crafted binary file, possibly related to a buffer
     underflow in fs/ext2.c in GNU GRUB 2.02.
   - Fix for CVE-2017-10929 (Closes: #867369)
     The grub_memmove function in shlr/grub/kern/misc.c in radare2 1.5.0
     allows remote attackers to cause a denial of service (heap-based buffer
     overflow and application crash) or possibly have unspecified other
     impact via a crafted binary file, possibly related to a read overflow
     in the grub_disk_read_small_real function in kern/disk.c in GNU GRUB
     2.02.
  * Switch to Debian Standard Version 4.0.0

 -- Sebastian Reichel <sre@debian.org>  Thu, 13 Jul 2017 00:05:39 +0200

radare2 (1.5.0+dfsg-1) experimental; urgency=medium

  * New upstream release

 -- Sebastian Reichel <sre@debian.org>  Wed, 31 May 2017 18:14:20 +0200

radare2 (1.4.0+dfsg-1) experimental; urgency=medium

  * New upstream release
    - Dropped cherry picked patches (applied upstream)

 -- Sebastian Reichel <sre@debian.org>  Tue, 18 Apr 2017 22:34:57 +0200

radare2 (1.3.0+dfsg-2) experimental; urgency=medium

  * Add upstream patch to fix CVE-2017-7274 (Closes: #858873)

 -- Sebastian Reichel <sre@debian.org>  Wed, 29 Mar 2017 00:53:13 +0200

radare2 (1.3.0+dfsg-1) experimental; urgency=low

  * New upstream release
    - Dropped cherry picked patches (applied upstream)

 -- Sebastian Reichel <sre@debian.org>  Mon, 13 Mar 2017 16:14:13 +0100

radare2 (1.2.1+dfsg-5) experimental; urgency=high

  * Add upstream patches to fix security bugs
    - CVE-2017-6415 (Closes: #856572)
      The dex_parse_debug_item function in libr/bin/p/bin_dex.c in radare2
      1.2.1 allows remote attackers to cause a denial of service (NULL
      pointer dereference and application crash) via a crafted DEX file.
    - CVE-2017-6387 (Closes: #856574)
      The dex_loadcode function in libr/bin/p/bin_dex.c in radare2 1.2.1
      allows remote attackers to cause a denial of service (out-of-bounds
      read and application crash) via a crafted DEX file.
    - CVE-2017-6319 (Closes: #856579)
      The dex_parse_debug_item function in libr/bin/p/bin_dex.c in radare2
      1.2.1 allows remote attackers to cause a denial of service (buffer
      overflow and application crash) or possibly have unspecified other
      impact via a crafted DEX file.
  * Add small patch from Graham Inggs to fix FTBFS when
    linked with as-needed (Closes: #856329)

 -- Sebastian Reichel <sre@debian.org>  Fri, 03 Mar 2017 06:24:35 +0100

radare2 (1.2.1+dfsg-4) experimental; urgency=high

  * Add upstream patch to fix security bug (Closes: #856063)
    - CVE-2017-6197
      The r_read_* functions in libr/include/r_endian.h in radare2 1.2.1
      allows remote attackers to cause a denial of service (NULL pointer
      dereference and application crash) via a crafted binary file, as
      demonstrated by the r_read_le32 function.

 -- Sebastian Reichel <sre@debian.org>  Sat, 25 Feb 2017 02:38:12 +0100

radare2 (1.2.1+dfsg-3) experimental; urgency=medium

  * Add upstream patch to fix hurd build

 -- Sebastian Reichel <sre@debian.org>  Thu, 09 Feb 2017 20:29:23 +0100

radare2 (1.2.1+dfsg-2) experimental; urgency=medium

  * Fix build on architectures without debugger support via cherry-picked
    upstream patch

 -- Sebastian Reichel <sre@debian.org>  Mon, 06 Feb 2017 19:01:59 +0100

radare2 (1.2.1+dfsg-1) experimental; urgency=medium

  * New upstream release
   - bugfixes
  * Drop patch (fixed upstream)

 -- Sebastian Reichel <sre@debian.org>  Sun, 05 Feb 2017 07:12:19 +0100

radare2 (1.2.0+dfsg-1) experimental; urgency=low

  * New upstream release
   - Lots of new features
   - misc. bugfixes
  * Add patch to fix build with capstone 3.0.4

 -- Sebastian Reichel <sre@debian.org>  Tue, 31 Jan 2017 22:36:51 +0100

radare2 (1.1.0+dfsg-1) unstable; urgency=medium

  * New upstream release
   - Fix ARM thumb code assembler (Closes: #844007)
   - preprocessor for ragg2 and rasm2
   - per-node folding support in graph
   - New commands (fla, pdc, pir, dmS, ?*, ~?.)
   - New variables ($fl)
   - autocompletion after @
   - implement bin.debase64
   - support for ARM64 PE binaries
   - libmagic fixes to avoid warnings with GNU libmagic
   - Greatly improves AVR support
   - and many more features and bugfixes...
  * Switch to github based watch file
  * Drop David from Uploaders list in request of MIA team (Closes: #845288)

 -- Sebastian Reichel <sre@debian.org>  Tue, 20 Dec 2016 14:27:33 +0100

radare2 (1.0.2+dfsg-1) unstable; urgency=medium

  * New upstream bugfix release
   - comes with Debian's FTBFS bugfix patches
   - fix commands "izz" & "aeim-"
   - some other minor fixes
  * Disable debugging on arm64 (Closes: #843939)
  * Build with --with-libversion=1.0, since 1.0.2 is ABI compatible with 1.0
  * Fix soname version of libr2.so via buildsystem patch

 -- Sebastian Reichel <sre@debian.org>  Fri, 11 Nov 2016 03:18:33 +0100

radare2 (1.0+dfsg-1) unstable; urgency=medium

  * New upstream version
    - Lots of new features & bugfixes
    - bash autocompletion support
  * DFSG relevant changes
    - m68k has been removed upstream
    - z80 plugin has been replaced with a GPLv3 licensed one
    - mach code has been replaced with LLVM code (BSD style license)
  * Debian Patches
    - all of them were applied upstream
    - introduce two new patches to fix FTBFS
  * Reproducible Build
    - upstream merged the source_epoch fix
    - additionally most occurrences of __FILE__ were removed upstream
  * Enable debugging support on arm64, ppc, ppc64, mips & mips64
  * Switch to compat level 10

 -- Sebastian Reichel <sre@debian.org>  Wed, 09 Nov 2016 05:45:27 +0100

radare2 (0.10.6+dfsg-1) unstable; urgency=medium

  * New upstream version
  * Add libkvm-dev build-dependency on kfreebsd to avoid FTBFS

 -- Sebastian Reichel <sre@debian.org>  Tue, 27 Sep 2016 05:27:05 +0200

radare2 (0.10.5+dfsg1-1) unstable; urgency=medium

  * Fixed DFSG source tarball
   - previously still contained the apple public source licensed mach code
  * Disable debugger on kfreebsd and hurd
  * Drop git build-depend
   - not needed with Debian's configuration

 -- Sebastian Reichel <sre@debian.org>  Thu, 01 Sep 2016 16:48:47 +0200

radare2 (0.10.5+dfsg-1) unstable; urgency=low

  * New upstream version (Closes: #778402, #748234, #766794):
    - Support for GameBoy added.
    - Support for Nintendo DS, NES, SNES, N64, DOL (Wii/Gamecube)
    - Support for Sega megadrive, SMS
    - Support for cr16, msp430, tms320, m68k, Spc700, Propeller
    - Support for i4004, LH5801, z80, pebble watch, xtensa, lanai, microblaze
    - Support for Capstone
    - Support for cryptography
    - Callgraph navigation view
    - Support for heap analysis (linux-glibc)
    - Improved PE & ELF parsers
    - DWARF support
    - ASCII Art graphs
    - Web Interface
    - Tons of bugs and segfaults fixed, some others added for sure.
    - ahi now supports IPv4 and syscall
  * debian/patches:
    - Dropped all old patches
    - Add a few new patches to fix manpage warnings from lintian
    - Disable m68k asm plugin to avoid linking GPL and BSD-4-clause code
    - Disable mach bin and z80 asm plugin (removed to be DFSG compatible)
    - add patch to generate GIT_NOW from SOURCE_DATE_EPOCH based on Chris
      Lamb's patch (Closes: #835262)
  * Dropped radare2-plugins package (included plugins are built statically)
  * Update Debian Standards Version to 3.9.8
  * Add dependency to zlib to avoid built-in library
  * Drop -dbg packages (switch to automatically generated -dbgsym)
  * Add get-orig-source rule, which creates dfsg version
  * Make myself the Maintainer again (ender is MIA)

 -- Sebastian Reichel <sre@debian.org>  Fri, 26 Aug 2016 15:42:57 +0200

radare2 (0.9.6-3.1) unstable; urgency=medium

  * debian/patches/12_fix_strcasestr_declaration: Add #define _GNU_SOURCE
    as well, to get the definition of strcasestr (closes: #735921).

 -- Steve McIntyre <93sam@debian.org>  Sun, 23 Mar 2014 14:38:44 +0000

radare2 (0.9.6-3) unstable; urgency=medium

  * debian/patches/12_fix_strcasestr_declaration: Fixed a missing include,
    thanks to Matthias Klose for pointing it (closes: #735921).

 -- David Martínez Moreno <ender@debian.org>  Tue, 28 Jan 2014 10:29:31 -0800

radare2 (0.9.6-2) unstable; urgency=low

  * debian/control: Switched Sebastian and myself as Maintainer/Uploader.
  * debian/patches/08_proper_tcc_build: Updated to fix a wrong ifeq in the
    Makefile.
  * debian/watch: Updated as it seems that pancake is releasing .tar.xz now.
  * debian/patches/01_fix-kfreebsd-compilation: Completely revamped from
    upstream commit 1941efc, to (hopefully) fix builds on kFreeBSD.
  * Added a lintian override for spelling-error-in-binary in libradare2-0.9.6,
    as it's invalid.

 -- David Martínez Moreno <ender@debian.org>  Fri, 17 Jan 2014 07:48:47 -0800

radare2 (0.9.6-1) unstable; urgency=low

  * New Year's release.  Just in time!
  * New upstream release.  Main changes are:
    - r2 now supports UTF-8, RGB and TrueColor ansi codes.
    - More platforms supported: ARM aarch64, TI c55x+, 8051, Javascript
      (emscripten), ARCcompact.
    - ASLR for PIE binaries is now supported by using the -B flag.
    - MACH0 XNU kernels are now properly loaded.
    - Added support for TE binaries.
    - Identify some PC BIOS and UEFI ROMs.
    - Java Class file parser has been rewritten to support Java7.
    - Python ctypes, D and Java JNI bindings.
    - An embedded webserver have been included, and you can invoke it using
      the `=h' or '=H' commands.
    - Improved JSON output for many commands. Just append 'j' to the command
      and it will use that format.
    - First release with an extensive test suite.
    - Lots of bugs has been fixed, overflows, memory leaks, and many
      handcrafted binaries can now be loaded without segfaults, crashes or
      lack of info (closes: #716192).
  * debian/patches:
    - 01_fix-kfreebsd-compilation: Refreshed.
    - 02_link-needed-libmagic: Fixed.
    - 03_unsafe_snprintf: New, partly taken from upstream commit 1289476 to
      fix an incomplete snprintf that was breaking the hardened build.
    - 04_remove_non-installable_library: New, to fix an extraneous t.0.9.6
      that was appearing in the builds.
    - 05_typos: Added, some typos and wrong man macros.
    - 06_no_forced_rpath: Added, disable rpath when --with-rpath is not
      present.
    - 07_propagate_ldflags: New, propagate LDFLAGS from environment into
      compilation by making assignment additive, fixing hardened builds.
    - 08_proper_tcc_build: Add .so as the default suffix of libr_tcc.
    - 09_fix_internal_plugins: New, fixed a bunch of problems with the
      internal bindings: made the lua check support Debian's lua, fixed
      library prefix, and removed ncurses bogus linking.
    - 10_fix_rafind2_segfault: New, backported from e0f4364 to fix a segfault
      reported by the Mayhem team (closes: #716503).
    - 11_block_libtcc_install: New, backported from upstream 1531e96 to stop
      installing libtcc files.
    - 101_split_plugins_installation: New, Debian-only, to ease packaging of
      the plugins.  Otherwise they ended up in libradare2.
  * debian/control:
    - Build-Depends on valabind (>> 0.7.4) and swig to generate internal
      plugins, plus python-dev and liblua5.2-dev for the corresponding
      extensions, and dh-exec to help with the packaging of radare2-plugins.
    - Updated descriptions.
    - Bumped Standards-Version to 3.9.5 (no changes).
    - Created another package (radare2-plugins) with the Python and Lua
      extensions (plus the dependency on Vala).
    - Make radare-plugins depend on valac; otherwise the vala extension is not
      functional.
  * debian/rules:
    - Fix a broken flag for configure (--without-debugger ->
      --disable-debugger) that was causing the debugger to be enabled back in
      several architectures where it was previously disabled, and thus breaking
      the builds (closes: #732853).
    - Force --fail-missing on dh_install to make sure that we don't miss new
      files in subsequent releases.
  * Removed the libradare2's symbols file because radare2 doesn't really try
    to have any sort of backwards compatibility.  It's futile to dump it from
    scratch in every release.

 -- David Martínez Moreno <ender@debian.org>  Tue, 31 Dec 2013 23:57:03 -0800

radare2 (0.9.4-2) unstable; urgency=low

  [ Sebastian Reichel ]
  * r_debug_desc_plugin_native is not available on some architectures,
    so make tag it accordingly in *.symbols file.
  * Add patch to fix compilation under GNU/kFreeBSD.

 -- David Martínez Moreno <ender@debian.org>  Tue, 10 Dec 2013 11:17:44 -0800

radare2 (0.9.4-1) unstable; urgency=low

  * New upstream release
  * Update debian/copyright
  * Bump Debian Standards Version 3.9.2 -> 3.9.4
  * Create new -common package for arch independent files

 -- Sebastian Reichel <sre@debian.org>  Sun, 22 Sep 2013 19:50:24 +0200

radare2 (0.9-3) unstable; urgency=low

  * Add Patch from Julian Taylor to fix FTBFS when
    building using ld --as-needed (Closes: #653873)

 -- Sebastian Reichel <sre@debian.org>  Sat, 31 Dec 2011 20:43:17 +0100

radare2 (0.9-2) unstable; urgency=low

  * readd fix in symbols file:
    r_debug_desc_plugin_native is only built for x86, x86-64 and arm

 -- Sebastian Reichel <sre@debian.org>  Tue, 06 Dec 2011 14:29:09 +0100

radare2 (0.9-1) unstable; urgency=low

  * New upstream release
   - automatic hurd detection
  * radare2 now recommends libradare2-dev
   - header files are used by ragg2-cc

 -- Sebastian Reichel <sre@debian.org>  Tue, 06 Dec 2011 01:36:55 +0100

radare2 (0.8.8-2) unstable; urgency=low

   * fix symbols file:
     r_debug_desc_plugin_native is only built for x86, x86-64 and arm
   * add patch to fix build on kFreeBSD
   * add upstream patch to fix a bug when redefining io sections

 -- Sebastian Reichel <sre@debian.org>  Fri, 04 Nov 2011 22:53:13 +0100

radare2 (0.8.8-1) unstable; urgency=low

  * new upstream release
   - fixed lintian warnings
  * update copyright information

 -- Sebastian Reichel <sre@debian.org>  Tue, 01 Nov 2011 13:03:36 +0100

radare2 (0.8.1-1) unstable; urgency=low

  * new upstream release
  * remove all patches (included upstream)
  * switch from cdbs to debhelper
  * update Debian Standards Version to 3.9.2
  * make libradare2 multi-arch capable

 -- Sebastian Reichel <sre@debian.org>  Sun, 24 Jul 2011 15:06:17 +0200

radare2 (0.7-3) unstable; urgency=low

  * update the fcntl patch
  * new patch: honor --without-debugger to fix build on unsupported
    architectures
  * new patch: add kfreebsd support

 -- Sebastian Reichel <sre@debian.org>  Wed, 13 Apr 2011 11:04:00 +0200

radare2 (0.7-2) unstable; urgency=low

  * disable debugger on all arches except i386, amd64, arm
    to make them building
  * new patch: drx_add / drx_del should not be exported
  * new patch: add fcntl.h to fix build error on kfreebsd / hurd
  * new patch: fix cflags to build some failing plugins
  * Closes: #621412

 -- Sebastian Reichel <sre@debian.org>  Mon, 04 Apr 2011 15:27:16 +0200

radare2 (0.7-1) unstable; urgency=low

  * New upstream release

 -- Sebastian Reichel <sre@debian.org>  Wed, 09 Mar 2011 00:54:02 +0100

radare2 (0.6-1) unstable; urgency=low

  * Initial Upload to Debian (Closes: #573345)

 -- Sebastian Reichel <sre@debian.org>  Mon, 07 Feb 2011 13:00:47 +0100
